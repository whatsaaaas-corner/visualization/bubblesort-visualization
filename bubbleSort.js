let height = 0;
let pivots = [];
let index;


async function bubbleSort(array) {
    if (array.length < 2) return array;

    let swap;
    let len = array.length - 1;

    do {
        swap = false;

        for (let i = 0; i < len; i++) {
            if (array[i] < array[i+1]) {
                await wait(350);
                pivots.splice(pivots.indexOf(i+1), 1);
                pivots.push(i+1);

                let t = array[i];
                array[i] = array[i+1];
                array[i+1] = t;

                swap = true;
                index = i+1;
            }
        }
        len--;
    } while(swap);

    pivots.splice(pivots.indexOf(index), 1);

    return array;
}

function setupArray(array) {
    let len = array.length;
    let element = document.querySelector('#scene');
    
    if (len == 0) return;

    let html = generateHTML(array);
    element.innerHTML = html;
}

function generateHTML(array) {
    if (pivots.length == 0) clearInterval(intervalId);

    let html = '';
    for (let i = 0; i < array.length; i++) {
        height = 10 * array[i];
        if (pivots.indexOf(i) > -1) {
            html += `
                <span class='red' style='height:${height}px'>${array[i]}</span>
            `;
        }
        else {
            html += `
                <span style='height:${height}px'>${array[i]}</span>
            `;
        }
    }

    return html;
}

function visualize(array) {
    for (let i = 0; i < array.length; i++) {
        setupArray(array);
    }
}

function wait(time) {
    return new Promise(resolve => setTimeout(resolve, time));
}