# bubblesort-visualization

Bubble sort, sometimes referred to as sinking sort, is a simple sorting algorithm that repeatedly steps through the list, compares adjacent pairs and swaps them if they are in the wrong order.
https://en.wikipedia.org/wiki/Bubble_sort

**DEMO**

![Demo](ezgif.com-crop.gif)
